CC = gcc
CFLAGS = -g -D DEBUG=1 -lm -std=c11
BIN = wattmetre-read wattmetre-read-v3

all: $(BIN)

wattmetre-read: wattmetre_read.c
	$(CC) -o $@ $< $(CFLAGS)

wattmetre-read-v3: v3/*.c v3/*.h
	$(CC) -o $@ -DDEBUG_ADD_FRAME_IN_CSV=1 -DDEBUG_DUMP_FRAME v3/wattmetre_main.c v3/wattmetre_lib.c $(CFLAGS) 

install:
	mkdir -p /usr/local/bin
	cp $(BIN) /usr/local/bin/

clean:
	rm $(BIN)
