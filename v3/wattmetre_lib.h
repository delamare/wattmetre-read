
#pragma once
#ifndef __WATTMETRE_LIB_
#define __WATTMETRE_LIB_

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L

#endif
//#ifndef _DEFAULT_SOURCE
//#define _DEFAULT_SOURCE
//#endif

#include <time.h>

#ifdef __cplusplus
extern "C"
{
#endif
// no more required
//void send_configuration_frame(int fd, int polling_period);

typedef struct timespec * timespec_p_t;

int my_get_next_char(int fd, unsigned char *buf);
int my_clock_gettime(clockid_t clk_id, timespec_p_t tp);

int read_device_values(int fd, int plug_count);

char *now(void); // For logging output 



#ifdef __cplusplus
}
#endif


#endif