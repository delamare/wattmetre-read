

// Required for sqrtf
#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <time.h>
#include <math.h> // sqrtf require _POSIX_C_SOURCE 200112L
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>

#include "wattmetre_lib.h"
#include "errno.h"

#ifdef _POSIX_C_SOURCE
#undef _POSIX_C_SOURCE
#endif

#define _ISOC99_SOURCE

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define AT __FILE__ ":" TOSTRING( __LINE__ ) 

// #define MAX_MODULE 16
// #define NB_VOIE_P_MODULE 6

typedef struct
{
  float current;
  float activepow;
} i_ui_t;

#define ERR_MSG_SIZE 2048
typedef struct
{
  bool   is_valid;
  char  err_msg[ERR_MSG_SIZE];
  struct timespec frame_received_timestamp;
  unsigned char *original_frame_without_header; // Cautious : Not end by \0
  int original_frame_without_header_buffer_size ;
  uint16_t len;
  uint16_t id;
  uint32_t delta_us;
  uint32_t sampleCount;
  float u[3];
  unsigned int val_count;
  i_ui_t val[]; // MAX_MODULE * NB_VOIE_P_MODULE];
} data_header_t;

#define FRAME_HEADER_SIZE 4
// Size for len to U records
#define MINIMAL_FRAME_SIZE (2 + 2 + 4 + 4 + (3 * 4))
// Size for each plug
#define DATA_IUI_UNIT_SIZE (4 + 4)
#define CRC_SIZE 1

#define ALL_MAX_FIX_SIZE (FRAME_HEADER_SIZE + MINIMAL_FRAME_SIZE + CRC_SIZE)

typedef enum
{
  SEARCH_HEADER = 0,
  WAIT_FD,
  WAIT_FC,
  WAIT_FB
} header_status_t;


char *now(void){
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  static char outbuf[128]="";
  strftime(outbuf , 128 , "%Y/%m/%d-%H:%M:%S;%c", &tm );
  return outbuf;
}

uint16_t extract_uint16(unsigned char *dst)
{
  return (uint16_t)((dst[1] << 8) | dst[0]);
}

uint32_t extract_uint32(unsigned char *dst)
{
  uint32_t resu = ((dst[3]) << 24) | ((dst[2]) << 16) | ((dst[1]) << 8) | dst[0];
  return resu;
}

float extract_float32(unsigned char *dst)
{
  uint32_t thei = extract_uint32(dst);
  float * fp = (float *) &thei;  // assuming target host is 32-bit IEEE 754 single precision floating point number
  return *fp; 
}

bool my_get_next_n_char(int fd, unsigned char *buf, int len)
{
  while (len > 0 && my_get_next_char(fd, buf++))
  {
    len--;
  }
  if (len == 0)
  {
    return true;
  }
  return false;
}

uint8_t compute_crc(unsigned char *buf, int count)
{
  uint8_t crc = 0;
  while (count > 0)
  {
    crc ^= *buf++;
    count--;
  }
  return crc;
}

// Got values in 1/ 2^32 secondes
// Need to x  (2^32 / 1000000) => need x 4295
// #define SHIFT_TO_USEC (((long)2^32)/1000000)
#define SHIFT_TO_USEC 4295
uint32_t to_usec(uint32_t delta) {          
  return (delta / SHIFT_TO_USEC);
}

void shift_timestamp(data_header_t *decodedp)
{
  uint32_t us = decodedp->delta_us;
  if (decodedp->frame_received_timestamp.tv_nsec >= us)
  {
    decodedp->frame_received_timestamp.tv_nsec -= us;
  }
  else
  {
    decodedp->frame_received_timestamp.tv_sec--;
    decodedp->frame_received_timestamp.tv_nsec = (1000000 - us) - decodedp->frame_received_timestamp.tv_nsec;
  }
}

void cleanupDecoded(data_header_t *decoded ) {
  decoded->is_valid = true;
  decoded->err_msg[0]='\0';
  decoded->delta_us=0;
  decoded->sampleCount=0;
  for (int i=0 ; i<3 ; i++){
    decoded->u[i]= NAN;
  }
  for (int i= 0 ; i <  decoded->val_count ; i++){
    decoded->val[i].activepow= NAN;
    decoded->val[i].current= NAN ;
  }
}

// dest  is dest_size char []
void appendStr(char *dest, int dest_size, char* msg, ... ){
  va_list ap;
  va_start(ap, msg);
  int i_cursize = strlen(dest);
  int maxlen = dest_size - i_cursize;
  vsnprintf(dest, maxlen, msg, ap);
  va_end(ap);

}

void analyse_nan(data_header_t *decoded)
{
  int nan_count = 0;
  if ( ! decoded->is_valid ) {
    return ;
  }
  for (int i = 0; i < 3; i++)
  {
    if (isnan(decoded->u[i]))
    {
      nan_count++;
    }
  }
  for (int i = 0; i < decoded->val_count; i++)
  {
      if ( isnan(decoded->val[i].current)){
      nan_count++;
    }
    if ( isnan(decoded->val[i].activepow)){
      nan_count++;
    }
  }
  if (nan_count > 0)
  {
    fprintf(stderr, "%s;NAN FOUND;%d nan found;FRAME=", now(), nan_count);

    for (int i = 0; i < (decoded->len + 3); i++)
    { // + 2 for len display  + 1 for crc
      fprintf(stderr, "%02x:", (unsigned char)decoded->original_frame_without_header[i]);
      if (((i + 1) % 4) == 0)
      {
        fprintf(stderr, " ");
      }
    }
    fprintf(stderr, "\n");
    fflush(stderr);
  }
}
// decoded.len is already fill
// crc is alread checked
bool fill_frame_struct(unsigned char *buf, data_header_t *decoded, int plug_count)
{
  unsigned char *buf_save = buf;
  cleanupDecoded(decoded);

  decoded->original_frame_without_header = buf;
  if (decoded->len < (MINIMAL_FRAME_SIZE -2) ) // minus len(2Bytes) field
  {
    fprintf(stderr, "%s;FRAME TO SMALL;ERROR : decoding frame. Len(%d) is lower or equal than MINIMAL_FRAME_SIZE(%d)\n",now(), decoded->len, MINIMAL_FRAME_SIZE);
    appendStr(decoded->err_msg, ERR_MSG_SIZE, "ERROR : decoding frame. Len(%d) is lower or equal than MINIMAL_FRAME_SIZE(%d) at " AT ". ", decoded->len, MINIMAL_FRAME_SIZE ) ;
    decoded->is_valid = false;
    return false;
  }
  int nbiui_records = (decoded->len - MINIMAL_FRAME_SIZE + 2) / 8; //  +2 : add len field,  8== 2*sizeof(floats32) per plug
  if (nbiui_records != plug_count)
  {
    fprintf(stderr, "%s;BAD PLUG RECORD COUNT IN FRAME;ERROR : decoding frame. plug_count require(%d) is not plug_cont found in frame(%d)\n",now(), nbiui_records, plug_count);
    appendStr(decoded->err_msg, ERR_MSG_SIZE, "ERROR : decoding frame. plug_count require(%d) is not plug_cont found in frame(%d) at " AT ". ", nbiui_records, plug_count ) ;
    return false;
  }

#ifdef DEBUG_DUMP_FRAME
  printf("\nProcessing Frame=\n");
  for (int i = 0; i < (decoded->len + 3); i++)
  { // + 2 for len display  + 1 for crc
    printf("%02x:", (unsigned char) decoded->original_frame_without_header[i]);
    if (((i+1) % 4) == 0)
    {
      printf(" ");
      fflush(stdout);
    }
  }
  printf("\n");
  fflush(stdout);
#endif

  buf+=2; // Skip len field

  decoded->id = extract_uint16(buf);
  buf += 2;
  uint32_t tmphex=extract_uint32(buf);
  decoded->delta_us = to_usec(tmphex);
  buf += 4;
  shift_timestamp(decoded);
  decoded->sampleCount = extract_uint32(buf);

  if (decoded->sampleCount == 0) {
    decoded->is_valid=false;
    decoded->sampleCount=1;
  }

  buf += 4;
  float f;
  for (int i = 0; i < 3; i++)
  {
    f = extract_float32(buf);
    buf += 4;
    decoded->u[i] = (isnan(f)) ? f : sqrtf(f / decoded->sampleCount);
  }
  int nbModule = nbiui_records / 6; // 6 records par modules
  for (int i = 0; i < nbModule; i++)
  {
    for (int j = 0; j < 6; j++)
    {
      f = extract_float32(buf);
      buf += 4;
      decoded->val[i * 6 + j].current = (isnan(f)) ? f : sqrtf(f / decoded->sampleCount);
    }
    for (int j = 0; j < 6; j++)
    {
      f = extract_float32(buf);
      buf += 4;
      decoded->val[i * 6 + j].activepow = (isnan(f)) ? f : (f / decoded->sampleCount);
    }
  }
  int read_size=buf - buf_save - 2 ; // -2 : minus 2 bytes for len field
  if (read_size > decoded->len )
  {
    fprintf(stderr, "%s;DECODING FRAME ERROR;ERROR : bad frame decoding.\n",now());
    appendStr(decoded->err_msg, ERR_MSG_SIZE, "ERROR : bad frame decoding." AT ". " );
    decoded->is_valid=false;
    return false;
  }

  analyse_nan(decoded);

  return true;
}

void csv_head(int plug_count)
{
  static bool already_show = false;
  if (already_show == false)
  {
    already_show = true;
    printf("#timestamp,#frame_is_ok,#U1,#U2,#U3");
    for (int i = 1; i <= plug_count; i++)
    {
      printf(",#current%d,#activepow%d", i, i);
    }
#ifdef DEBUG
    printf(",frame_len2,frame_id2,frame_delta4,frame_sample_count4,message");
#ifdef DEBUG_ADD_FRAME_IN_CSV
    printf(",frame_dump");
#endif

#endif

    printf("\n");
  }
}

void csv_dump_one_line(data_header_t *decodedp)
{
  printf("%lld.%.9ld", (long long)decodedp->frame_received_timestamp.tv_sec, decodedp->frame_received_timestamp.tv_nsec);
  printf(",%s", (decodedp->is_valid) ? "true" : "false");

  for (int i = 0; i < 3; i++)
  {
    if (decodedp->is_valid)
    {
      printf(",%.1f", decodedp->u[i]);
    }
    else
    {
      printf(",");
    }
  }
  for (int i = 0; i < decodedp->val_count; i++)
  {
    if (decodedp->is_valid)
    {
      printf(",%.1f,%.1f", decodedp->val[i].current, decodedp->val[i].activepow);
    }
    else
    {
      printf(",,");
    }
  }
#ifdef DEBUG

  for (int i=strlen (decodedp->err_msg) -1 ; i>=0 ; i--) {
    if (decodedp->err_msg[i] == ',' || decodedp->err_msg[i] == '\n') {
      decodedp->err_msg[i] = '.';
    }
  }
  printf(",%d,%d,%d,%d,%s", decodedp->len, decodedp->id, decodedp->delta_us, decodedp->sampleCount, decodedp->err_msg);

#ifdef DEBUG_ADD_FRAME_IN_CSV
  printf(",");
  if (decodedp->original_frame_without_header)
  {
    for (int i = 0; i < (decodedp->len + 2 + 1); i++) // +2 for adding len field, +1 for addgin CRC field
    {
      printf("%02x:", (unsigned char)decodedp->original_frame_without_header[i]);
    }
  }
#endif
#endif

  printf("\n");
}

int compute_frameMaxSize(int plug_count)
{
  return ALL_MAX_FIX_SIZE + (plug_count * DATA_IUI_UNIT_SIZE) + 2;
}
int alloc_all_buffers(int plug_count, int *fram_buf_sizep, unsigned char **frame_buf_h, data_header_t **decoded_framh)
{

  *fram_buf_sizep = compute_frameMaxSize(plug_count);
  unsigned char tempo[2];
  if (*frame_buf_h != NULL)
  {
    memcpy(tempo, *frame_buf_h,2); // Len was present in 
    free(*frame_buf_h);
  }
  *frame_buf_h = malloc(*fram_buf_sizep);
  if (*frame_buf_h == NULL)
  {
    perror("Can't allocate buffer for frame reading. Exit\n");
    return 1;
  }
  memcpy( *frame_buf_h,tempo,2);
  struct timespec old;
  uint16_t oldlen=0;
  if (*decoded_framh != NULL)
  {
    old = (*decoded_framh)->frame_received_timestamp;
    oldlen = (*decoded_framh )->len;
    free(*decoded_framh);
  }
  (*decoded_framh) = (data_header_t *)malloc(sizeof(data_header_t) + sizeof(i_ui_t) * plug_count);
  if (*decoded_framh == NULL)
  {
    perror("Can't allocate memory for decoding frame. Exit\n");
    fprintf(stderr, "%s;MEMORY ERROR;CRITICAL : Can't allocate memory for decoding frame. Exit\n",now());
    appendStr((*decoded_framh)->err_msg, ERR_MSG_SIZE, "CRITICAL : Can't allocate memory for decoding frame. Exit at " AT ". ") ;

    return 1;
  }
  (*decoded_framh)->val_count = plug_count;
  cleanupDecoded(*decoded_framh);
  (*decoded_framh)->frame_received_timestamp = old;
  (*decoded_framh)->len = oldlen;
  (*decoded_framh)->original_frame_without_header_buffer_size = *fram_buf_sizep ; 
  (*decoded_framh)->original_frame_without_header = *frame_buf_h;
  return 0;
}

int read_device_values(int fd, int plug_count)
{
  int ret;
  unsigned char buf;
  int frame_max_size=0;
  unsigned char *frame_buf=NULL;

  header_status_t status = SEARCH_HEADER;

  data_header_t *decoded_framep = NULL;

  if (alloc_all_buffers(plug_count, &frame_max_size, &frame_buf, &decoded_framep))
  {
    close(fd);
    return 1;
  }

  if (sizeof(float) != 4)
  {
    fprintf(stderr , "%s;BAD FLOAT SIZE;ERROR : Your float  not 32 bits len. I need 32bits len to convert is 32-bit IEEE 754 single precision floating point number\n",now());
    close(fd);
    return 1;
  }

  if (plug_count != 0)
  {
    csv_head(plug_count);
  }

  // Read tty device
  while (my_get_next_char(fd, &buf) > 0)
  {
    switch (status)
    {
    case SEARCH_HEADER:
      if (buf == 0xFE)
      {
#ifdef DEBUG
//        printf("\n");
        fflush(stdout);
#endif
        status = WAIT_FD;
        cleanupDecoded(decoded_framep);
        ret = my_clock_gettime(CLOCK_REALTIME, &(decoded_framep->frame_received_timestamp));
        if (ret)
        {
          int myerr=errno;
//          perror("error when calling gettime");
          fprintf(stderr , "%s;GET TIME ERR; error durring gettime(). Errno=%d (%s)\n",now(),myerr,strerror(myerr));
        }
      }
      break;

    case WAIT_FD:
      if (buf == 0xFD)
      {
        status = WAIT_FC;
      }
      else
      {
        status = SEARCH_HEADER;
      }
      break;

    case WAIT_FC:
      if (buf != 0xFC)
      {
        status = SEARCH_HEADER;
        break;
      }
      else
      {
        status = WAIT_FB;
        break;
      }

    case WAIT_FB:
      if (buf != 0xFB)
      {
        status = SEARCH_HEADER;
        break;
      }
      if (my_get_next_n_char(fd, frame_buf, 2) != true)
      {
//        fprintf(stderr, "ERROR : Can't read length of frame.\n");
        fprintf(stderr , "%s;BAD FRAME LEN; Can't read length of frame. ",now());

        appendStr(decoded_framep->err_msg, ERR_MSG_SIZE, "ERROR : Can't read length of frame " AT ". " );
        csv_dump_one_line(decoded_framep);
        status = SEARCH_HEADER;
        break;
      }
      decoded_framep->len = extract_uint16(frame_buf);
      if (plug_count == 0) // Want Guess plug count + realloc struct
      {
        plug_count = (decoded_framep->len - (MINIMAL_FRAME_SIZE - 2)) / DATA_IUI_UNIT_SIZE;
        if (alloc_all_buffers(plug_count, &frame_max_size, &frame_buf, &decoded_framep))
        {
          close(fd);
          fprintf(stderr,"%s;ERROR allocating buffers.",now());
          return 1;
        }
        csv_head(plug_count);
        fprintf(stderr , "%s;INFO;detect '%d' plugs.",now(),plug_count);
      }
      int wantedlen = (MINIMAL_FRAME_SIZE - 2 + (DATA_IUI_UNIT_SIZE * plug_count));
      if (decoded_framep->len != wantedlen) // -2 : Don't count len field
      {
        fprintf(stderr, "%s;BAD FRAME LEN; ERROR : read a strange frame len (%d) want(%d) for (%d)plug. skip frame\n", now(), decoded_framep->len, wantedlen, plug_count);
        appendStr(decoded_framep->err_msg, ERR_MSG_SIZE, "ERROR : read a strange frame len (%d) want(%d) for (%d)plug. skip frame " AT ". ", decoded_framep->len, wantedlen, plug_count );
        decoded_framep->is_valid = false;
        if ((decoded_framep->len + 1 + 2 ) > decoded_framep->original_frame_without_header_buffer_size) {
          decoded_framep->len  = decoded_framep->original_frame_without_header_buffer_size - 3;
        }
        my_get_next_n_char(fd, frame_buf + 2, decoded_framep->len + 1);        
        csv_dump_one_line(decoded_framep);
        status = SEARCH_HEADER;
        break;
      }
      if ((decoded_framep->len + 2) > frame_max_size)
      {
        fprintf(stderr, "%s;FRAME TO LONG; ERROR : frame len (%d) is over pre-allocated buffer (%d). skip frame\n",now(), (decoded_framep->len + 2), frame_max_size);
        appendStr(decoded_framep->err_msg, ERR_MSG_SIZE, "ERROR : frame len (%d) is over pre-allocated buffer (%d). skip frame " AT ". ", (decoded_framep->len + 2), frame_max_size );
        decoded_framep->is_valid = false;
        csv_dump_one_line(decoded_framep);
        status = SEARCH_HEADER;
        break;
      }
      if (!my_get_next_n_char(fd, frame_buf + 2, decoded_framep->len + 1))
      { // Start at frame + 2 to keep len value 
        // +1 for crc read
        fprintf(stderr, "%s;CANT READ FRAME;ERROR : Can't read required len (%d) for this frame. skip\n",now(), (decoded_framep->len + 1));
        appendStr(decoded_framep->err_msg, ERR_MSG_SIZE, "ERROR : Can't read required len (%d) for this frame. skip. " AT ". ", (decoded_framep->len + 1) );
        decoded_framep->is_valid = false;
        csv_dump_one_line(decoded_framep);
        status = SEARCH_HEADER;
        break;
      }
      uint8_t theCrc = compute_crc(frame_buf , decoded_framep->len + 2); // Crc on all len + data
      if (theCrc != frame_buf[decoded_framep->len + 2])
      {
        fprintf(stderr, "%s;BAD FRAME CRC;ERROR : bad CRC on frame got=%02x, compute=%02x:. skip\n",now(), (unsigned char) frame_buf[decoded_framep->len + 2], (unsigned char) theCrc);
        decoded_framep->is_valid=false;
        appendStr(decoded_framep->err_msg, ERR_MSG_SIZE, "ERROR : bad CRC on frame got=%02x, compute=%02x. skip. " AT ". ", (unsigned char) frame_buf[decoded_framep->len + 2], (unsigned char) theCrc );
        csv_dump_one_line(decoded_framep);
        status = SEARCH_HEADER;
        break;
      }
      if ( fill_frame_struct(frame_buf, decoded_framep, plug_count) == false )
      {
        fprintf(stderr, "%s;BAD FRAME FORMAT; WARNING Wrong frame format. skip\n",now());
        appendStr(decoded_framep->err_msg, ERR_MSG_SIZE, "WARNING : Wrong frame format. skip. " AT ". " );
        decoded_framep->is_valid = false;
        csv_dump_one_line(decoded_framep);
        status = SEARCH_HEADER;
        break;
      }
      // Need to Dump structure as we want
      csv_dump_one_line(decoded_framep);
      status = SEARCH_HEADER;
    }
  }

  if (ret == -1)
  {
    int myerr=errno;
    fprintf(stderr , "%s;ERROR DURING READING TTY; error durring read (ret -1). Errno=%d (%s)\n",now(),myerr,strerror(myerr));

    exit(1);
  }
  close(fd);
  fprintf(stderr,"%s;ERROR uart loop interrupted.",now());
  return 0;
}
