// Compile with
// gcc -std=c11 -DDEBUG=1 -DDEBUG_ADD_FRAME_IN_CSV=1 -DDEBUG_DUMP_FRAME -o wattmetre-read wattmetre_main.c wattmetre_lib.c -lm
// or cmake ...

//#define _POSIX_C_SOURCE 199309L
#define _POSIX_C_SOURCE 200112L
// For cmakeraw...
#define _DEFAULT_SOURCE

#include "wattmetre_lib.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <time.h>
#include <string.h>


#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>

#define BAUD B921600

int configure_tty(char *devtty, unsigned int baud)
{
  int fd, ret;
  speed_t speed = baud;
  struct termios options;

  if (strcmp("-", devtty) == 0)
  {
    return 0; // unix default stdin fd
  }
  int isFile=0;
  if (strncmp("file:",devtty,5) == 0){
    isFile=1;
    devtty+=5; // Skip file:
  }

  if ((fd = open(devtty, O_RDWR | O_NOCTTY)) == -1)
  {
    perror("Error while openning TTY device");
    exit(1);
  }

  if (isFile){return fd;}

  //fcntl(fd, F_SETFL, 0);
  if ((ret = tcgetattr(fd, &options)) != 0)
  {
    perror("Error while getting terminal's attributes");
    exit(1);
  }
  if ((ret = cfsetispeed(&options, speed)) == -1)
  {
    perror("Error while setting input speed");
    exit(1);
  }
  if ((ret = cfsetospeed(&options, speed)) == -1)
  {
    perror("Error while setting output speed");
    exit(1);
  }
  cfmakeraw(&options);
  options.c_cflag |= (CLOCAL | CREAD);
  options.c_cflag &= ~CRTSCTS;
  if (tcsetattr(fd, TCSANOW, &options) != 0)
  {
    perror("Error while setting terminal's attributes");
    exit(1);
  }
  return fd;
}

int configure_tty_new(char *devtty, unsigned int baud)
{
  int fd, ret;
  speed_t speed = baud;
  struct termios options;

  if (strcmp("-", devtty) == 0)
  {
    return 0; // unix default stdin fd
  }

  if ((fd = open(devtty, O_RDONLY | O_NOCTTY)) == -1)
  {
    perror("Error while openning TTY device");
    exit(1);
  }
  //fcntl(fd, F_SETFL, 0);
  if ((ret = tcgetattr(fd, &options)) != 0)
  {
    perror("Error while getting terminal's attributes");
    exit(1);
  }

  cfmakeraw(&options);
  if (tcsetattr(fd, TCSANOW, &options) != 0)
  {
    perror("ERROR : Can't set raw mode\n");
    exit(1);
  }
  // Aucun traitement et 1 par 1
  // termios_p->c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP
  //                          | INLCR | IGNCR | ICRNL | IXON);
  //          termios_p->c_oflag &= ~OPOST;
  //          termios_p->c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
  //          termios_p->c_cflag &= ~(CSIZE | PARENB);
  //          termios_p->c_cflag |= CS8;
  options.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
  if (tcsetattr(fd, TCSANOW, &options) != 0)
  {
    perror("Can't switch to raw mode - no echoline...\n");
    exit(1);
  }
  // Turn off characters processins
  options.c_cflag &= ~(CSIZE | PARENB);
  options.c_lflag |= CS8;
  if (tcsetattr(fd, TCSANOW, &options) != 0)
  {
    perror("Turn off char processing\n");
    exit(1);
  }

  // Only One char for read
  //  options.c_cc[VMIN]  = 1;
  // options.c_cc[VTIME] = 0;
  if (tcsetattr(fd, TCSANOW, &options) != 0)
  {
    perror("Can't switch to only one Char for read\n");
    exit(1);
  }

  if (!(cfsetispeed(&options, speed) == 0 && tcsetattr(fd, TCSANOW, &options) == 0))
  {
    perror("Error while setting input speed");
    exit(1);
  }

  if (!(cfsetospeed(&options, speed) == 0 && tcsetattr(fd, TCSANOW, &options) == 0))
  {
    perror("Error while setting output speed");
    exit(1);
  }

  if (tcgetattr(fd, &options) != 0)
  {
    perror("Can't read serial port parameter during configuration\n");
    exit(1);
  }
  if (cfgetispeed(&options) != speed)
  {
    perror("Strange : get current speed is no requiest speed\n");
    exit(1);
  }
  options.c_cflag |= (CLOCAL | CREAD);
  if (tcsetattr(fd, TCSANOW, &options) != 0)
  {
    perror("Can't set c_cflag with CREAD|CLOCAL\n");
    exit(1);
  }
  options.c_cflag &= ~CRTSCTS;
  if (tcsetattr(fd, TCSANOW, &options) != 0)
  {
    perror("Error while setting terminal's attributes - clear RTS/CTS");
    exit(1);
  }
  if (tcgetattr(fd, &options) != 0)
  {
    perror("Error while getting terminal's attributes");
    exit(1);
  }

  return fd;
}

// Got next char in serial port

int my_get_next_char(int fd, unsigned char *buf)
{
  int resu = read(fd, buf, 1);
#ifdef DEBUG_DUMP_FRAME
  // Dump frame content coming from device
  printf("%02x:", *buf);
  fflush(stdout);
#endif
  return resu;
}

int my_clock_gettime(clockid_t clk_id, timespec_p_t tp)
{

  int ret = clock_gettime(CLOCK_REALTIME, tp);
  if (ret)
  {
    perror("error when calling gettime");
  }
  return ret; // OK
}

char * outputlogfile="";
char * ttydev="";
long  plugcount=36;
int   verbose_level=0;

void help(char **argv)
{
    printf("%s : extract omegawatt datas from serial device\n", argv[0]);
    printf("%s --tty=tty-device --nb=plug-count [--log=your_log_file]\n", argv[0] );

    printf(" -t|--tty=your_tty_device : set input tty device (ie:/dev/ttyUSBxx). current is %s. Set to '-' to got stdin. \n\t--tty=file:YOUR_FILE of read YOUR_FILE as raw tty  \n", ttydev);
    printf(" -n|--nb=plug_count : set the required plug count. current is %ld. Set to 0 for autodetect on fiste read line.\n", plugcount);    
    printf(" -l|--log=your_log_file : set output log file. current is %s. Set to /dev/null to discard logs.\n", outputlogfile);
    printf(" -h|--help : This help\n");
}
int main(int argc, char *argv[])
{
  int fd;
//  long plug_count = 36; // 0 -> Autodetect on first frame
  char *end;
  int opterr = 0;

static struct option long_options[] =
        {
            /* These options set a flag. */
//            {"verbose", no_argument, 0, 'v'},
            {"help", no_argument, 0, 'h'},
            {"tty", required_argument, 0, 't'},            
            {"nb", required_argument, 0, 'n'},
            {"log", required_argument, 0, 'l'},
            {0, 0, 0, 0}};

  /* getopt_long stores the option index here. */
    int option_index = 0;
    int c;
    while (1)
    {
      c = getopt_long(argc, argv, "hvt:n:l:", // x: => Argument mandatory for -x
                      long_options, &option_index);
      if (c == -1)
      {
        break;
      }
      switch (c)
      {

      case 'h':
        help(argv);
        exit(1);
      case 'v':
        verbose_level++;
        break;
      case 't':
        ttydev = optarg;
        break;
      case 'n':
      plugcount = strtol(optarg, &end, 10);
      if (*end != '\0' || errno == ERANGE || errno == EINVAL )
      {
          fprintf(stderr , "ERROR : Can't convert argument --nb=%s to number\n.",optarg);
          help(argv);
          exit(1);
      }
        break;
      case 'l':
        outputlogfile = optarg;
        break;
      default:
        abort();
      }
    };

  if (optind != argc) {
    help(argv);
    fprintf (stderr , "ERR : Can't understand arguments");
    exit(1);
  }

  if ((plugcount % 6) != 0) {
      fprintf(stderr, "ERROR : plug_count should be multiple of 6\n");
      help(argv);
      exit(1);
  }



  if (strcmp(ttydev,"")== 0) {
    printf("ERROR : --tty=xxx is required\n");
    help(argv);
    exit(1);
  }
  fd = configure_tty(ttydev, BAUD);

  // if (argc > 2)
  // {
  //   plug_count = strtol(argv[2], &end, 10);
  //   if (*end != '\0')
  //   {
  //     fprintf(stderr, "Error while parsing plug count argument (int is required)\n");
  //     exit(1);
  //   }
  //   if ((plug_count % 6) != 0)
  //   {
  //     fprintf(stderr, "ERROR : plug_count should be multiple of 6\n");
  //     exit(1);
  //   }
  // }

  // if (argc > 3) {
  //   polling_period = strtol(argv[3], &end, 10);
  //   if (*end != '\0') {
  //     fprintf(stderr, "Error while parsing polling period argument (int is required)\n");
  //     exit(1);
  //   }
  // }

   if (strcmp(outputlogfile,"") != 0 && 
        freopen(outputlogfile,"a+", stderr) == NULL) {
     perror("Can't open file for logging\n");
     printf("ERROR : Can't open '%s' for logging.\n",outputlogfile);     
   }
  setbuf(stderr, NULL); // no buffering on stderr
  fprintf(stderr, "%s;INFO;Starting logging on '%s' plugcount=%ld\n",now(), ttydev, plugcount);
//  fflush(stderr);

  exit(read_device_values(fd, plugcount));
  
}
